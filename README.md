# Persistent Identifiers (PIDs)

The FAIR Data Principles require that “(meta)data are assigned a
globally unique and persistent identifier”.[^1]  This is important in
order to be able to reference the data from publications and to follow
those references and actually find the data.  The attribution of PIDs
to the data created at PaN facilities has become a widely adopted
standard.  However, the quality of the metadata registered in the PID
record still leaves room for improvement in many cases.  In
particular, we also need to include references to other entities in
the metadata: the scientists involved in creating a data publication,
the research organization that they are affiliated with, the
instrument that has been used to collect the data, the experimental
technique, the sample that has been measured, the software that has
been used to process the data and many things more.  All of these need
to be properly identified and in order to do that in a reliable
manner, they need to be referenced by their respective persistent
identifier.[^2]  Some of these PIDs are well established (e.g. ORCIDs
for researchers), some are ready to be used and just need wider
adoption (e.g. PIDs for instruments), some of them are just emerging
and need more development in the context of PaN facilities
(e.g. sample PIDs).

[^1]: Wilkinson, M. D. _et al_. (2016) 
	“The FAIR Guiding Principles for scientific data management and
	stewardship,” _Scientific Data_, 3(1). 
	doi: [10.1038/SDATA.2016.18](https://doi.org/10.1038/SDATA.2016.18).
	
	
[^2]: Bunakov, V. _et al_. (2022) 
	“Advanced infrastructure for PIDs in Photon and Neutron RIs.” 
	doi: [10.5281/zenodo.5905351](https://doi.org/10.5281/zenodo.5905351).
